- 1.5.0 (23 Dec. 2024): Updated for 1.21.4
- 1.4.0 (23 Nov. 2024):
  - Updated for 1.21.2-1.21.3
  - Substantial internal changes
- 1.3.2 (5 Sep. 2024): Marked as compatible with 1.21.1
- 1.3.1 (21 Jul. 2024):
  - Fixed an issue where deny lists would get skipped if there was no allow list
  - Added optional [SSS Translate](<https://modrinth.com/mod/sss-translate>) dependency;
  install [SSS Translate](<https://modrinth.com/mod/sss-translate>) for automatic translation updates (client-side)
- 1.3.0 (15 Jun. 2024):
  - Updated for 1.21
  - Updated translations
  - Minor improvements
- 1.2.15 (11 May 2024):
  - Marked as compatible with 1.20.6
  - Improved [Mod Menu](https://modrinth.com/mod/modmenu) integration
- 1.2.14 (27 Apr. 2024):
  - Updated for 1.20.5
  - Improved "ITEM_USE" target: it should behave as expected in more situations
  - Minor internal changes
- 1.2.13 (15 Apr. 2024): the ENTITY_ON_INTERACT target now works with non-living entities, fixes 
[#6](https://gitlab.com/supersaiyansubtlety/dispenser_configurator/-/issues/6)
- 1.2.12 (2 Feb. 2024):
  - Marked as compatible with 1.20.3 and 1.20.4
  - Minor internal changes
- 1.2.11 (19 Nov. 2023): Updated for 1.20.2
- 1.2.10 (21 Jun. 2023): Updated for 1.20 and 1.20.1!
- 1.2.9 (31 Mar. 2023): Updated for 1.19.3 and 1.19.4
- 1.2.8 (9 Aug. 2022): Marked as compatible with 1.19.2
- 1.2.7 (28 Jul. 2022):
  - Updated for 1.19.1
  - Minor internal changes
- 1.2.6 (25 Jul. 2022):
  - Fixed a bug that caused an error to be logged and prevented placement of certain blocks.
  - Added the following to the place_blocks configurator: 
    "pointed_dripstone",
    "small_dripleaf",
    "big_dripleaf",
    "glow_lichen",
    "#wool_carpets",
    "moss_carpet",
    "moss_block",
    "glow_berries",
    "hanging_roots"
- 1.2.5 (6 Jul. 2022):
  - Added behaviors for buckets and cauldrons, and buckets milking cows!
  - Fixed a bug that could cause certain behaviors to delete items. 
  - Fixed a bug that caused errors to log if a configurator was for multiple items and had an allow/deny list specified.
- 1.2.4 (5 Jul. 2022): Add a null check that logs an error, fixing compatibility with Rug
- 1.2.3 (23 Jun. 2022): Updated for 1.19!
- 1.2.2 (14 Jun. 2022): 

  Updated for 1.18.2.

  For builtin configurators, made is so that if an item cannot be dispensed 
(for example of a rail couldn't be placed because there's no supporting block), 
the dispensation fails, rather than the item be ejected.

- 1.2.1 (11 Dec. 2021):

  Marked as compatible with 1.18.1.
  
  Simplified the way configurators are loaded (thanks to insight from [LambdaAurora](https://lambdaurora.dev/)!)

- 1.2.1-b1 (10 Dec. 2021): Updated for 1.18!
- 1.2-b1 (10 Dec. 2021):

  This is a ***beta*** version. I've made substantial internal changes, and so 
  there's an increased likelihood that I've introduced bugs. Please report any bugs you
  find on the [issue tracker](https://gitlab.com/supersaiyansubtlety/dispenser_configurator/-/issues).
  
  - Added two new targets:
    - "DROP": ejects the item like a dropper
    - "FAIL": do nothing and stop any further behavior attempts
    
    These targets allow you to more easily disable and override existing behaviors.
    
    They can also be used to customize what happens when a configurator behavior fails.
    
    Thanks to [snowikit](https://www.curseforge.com/members/snowikit/projects) for the 
    feedback that led to the addition of these targets!

- 1.1.1 (20 Oct. 2021): Removed a static world reference, fixing compatibility with [Dimensional Threading](https://github.com/WearBlackAllDay/DimensionalThreading).

- 1.1 (17 Oct. 2021):
  
  - Completely rewrote how configurators are managed... twice.
  - `white`/`black` lists have been renamed to `allow`/`deny` lists. `white` and `black` lists are still recognized so old datapacks won't break. 
  - There is no more registry replacement (less log spam and chance for conflicts)
  - Fixed allow/deny lists not actually having any effect (sorry about that)
  - The way multiple dispenser behaviors for the same item has been greatly improved (whether they're added by this mod, other mods, or vanilla)
    - Before there could only be one behavior per item, now there can be many behaviors, and each is tried until one succeeds
    - Configurator datapacks may now specify `"exclusive" = true` to bypass the above behavior, meaning only that behavior will be tried
    - Multiple exclusive configurators for the same item may be specified so long as their allow/deny lists don't overlap

- 1.0.2 (12 Oct. 2021): 
  *Finally* updated for 1.17(.1)!
  Note that this version (and possibly others) don't work with [Dimensional Threading](https://github.com/WearBlackAllDay/DimensionalThreading/releases).
- 1.0.1 (16 Jan. 2021): No longer requires the newest version of the Fabric mod loader.
- 1.0 (16 Jan. 2021): First full release!
  Marked as compatible with 1.16.5.
- 0.0.4-beta-1 (12 Nov. 2020): New target: "PLACE_BLOCK". Updated default datapack so that blocks that can be either placed or 
  used to feed animals can now do either from a dispenser. 
- 0.0.3 (6 Nov. 2020): Removed unused mixins, added fix for vanilla bug that occurs when a Beehive 
is used while no players are around (not usually possible), removed shears entry from the default 
datapack because it messes with the way shears in dispensers interact with Beehives. 
- 0.0.2 (4 Nov. 2020): Initial ***alpha*** release. 
- 0.0.1 (4 Nov. 2020): Initial changelog entry, almost ready for ***alpha*** release. 