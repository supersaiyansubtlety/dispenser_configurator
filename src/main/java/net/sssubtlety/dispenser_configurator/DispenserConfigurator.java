package net.sssubtlety.dispenser_configurator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import net.minecraft.util.Identifier;

/*
* TODO:
*  - datagen
*  - gametest
*/
public final class DispenserConfigurator {
	public static final String NAMESPACE = "dispenser_configurator";

	public static final Identifier ID = Identifier.of(NAMESPACE, NAMESPACE);

	public static final Logger LOGGER = LoggerFactory.getLogger(NAMESPACE);
}
