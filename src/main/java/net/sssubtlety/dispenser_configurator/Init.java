package net.sssubtlety.dispenser_configurator;

import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.event.lifecycle.v1.CommonLifecycleEvents;
import net.fabricmc.fabric.api.resource.ResourceManagerHelper;
import net.fabricmc.fabric.api.resource.ResourcePackActivationType;
import net.fabricmc.loader.api.FabricLoader;
import net.fabricmc.loader.api.ModContainer;

import net.minecraft.resource.ResourceType;
import net.minecraft.util.Identifier;

import net.sssubtlety.dispenser_configurator.behavior.ConfiguratorManager;

import static net.sssubtlety.dispenser_configurator.DispenserConfigurator.NAMESPACE;

public class Init implements ModInitializer {
    @Override
    public void onInitialize() {
        ResourceManagerHelper.get(ResourceType.SERVER_DATA).registerReloadListener(ConfiguratorManager.INSTANCE);

        final ModContainer thisMod = FabricLoader.getInstance().getModContainer(NAMESPACE).orElseThrow();
        ResourceManagerHelper.registerBuiltinResourcePack(
            Identifier.of(NAMESPACE, "default_dispenser_configurators"),
            thisMod,
            ResourcePackActivationType.DEFAULT_ENABLED
        );

        CommonLifecycleEvents.TAGS_LOADED.register((registries, client) -> {
            if (!client) {
                ConfiguratorManager.INSTANCE.resolveBehaviors(registries);
            }
        });
    }
}
