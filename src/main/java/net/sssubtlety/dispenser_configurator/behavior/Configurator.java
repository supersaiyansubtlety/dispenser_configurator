package net.sssubtlety.dispenser_configurator.behavior;

import net.sssubtlety.dispenser_configurator.behavior.target.abstraction.DispenserBehaviorTarget;
import net.sssubtlety.dispenser_configurator.behavior.target.BlockOnUseTarget;
import net.sssubtlety.dispenser_configurator.behavior.target.ItemUseOnBlockTarget;
import net.sssubtlety.dispenser_configurator.behavior.target.DropTarget;
import net.sssubtlety.dispenser_configurator.behavior.target.FailTarget;
import net.sssubtlety.dispenser_configurator.behavior.target.EntityOnInteractTarget;
import net.sssubtlety.dispenser_configurator.behavior.target.ItemUseOnEntityTarget;
import net.sssubtlety.dispenser_configurator.behavior.target.ItemUseTarget;
import net.sssubtlety.dispenser_configurator.behavior.target.PlaceBlockTarget;
import net.sssubtlety.dispenser_configurator.util.CodecUtil;
import net.sssubtlety.dispenser_configurator.util.TaggableId;

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;

import net.minecraft.registry.DynamicRegistryManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.dynamic.Codecs;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;
import com.mojang.serialization.MapCodec;
import com.mojang.serialization.codecs.RecordCodecBuilder;

import java.util.Optional;

public record Configurator(
    ImmutableSet<DispenserBehaviorTarget.Creator> targetCreators, ImmutableSet<TaggableId> itemIds,
    int priority,
    Optional<ImmutableSet<TaggableId>> allowedBlockIds, Optional<ImmutableSet<TaggableId>> deniedBlockIds,
    Optional<ImmutableSet<TaggableId>> allowedEntityIds, Optional<ImmutableSet<TaggableId>> deniedEntityIds
) {
    public static final int DEFAULT_PRIORITY = 0;
    public static final int MIN_PRIORITY = Integer.MIN_VALUE / 2;
    public static final int MAX_PRIORITY = Integer.MAX_VALUE / 2;

    private static final ImmutableMap<String, DispenserBehaviorTarget.Creator> TARGET_CREATORS = ImmutableMap.ofEntries(
        new DispenserBehaviorTarget.Creator.Unpredicated(
            PlaceBlockTarget.NAME, PlaceBlockTarget::getInstance, true
        ).toEntry(),
        new DispenserBehaviorTarget.Creator.BlockPredicated(
            BlockOnUseTarget.NAME, BlockOnUseTarget::new, true
        ).toEntry(),
        new DispenserBehaviorTarget.Creator.BlockPredicated(
            ItemUseOnBlockTarget.NAME, ItemUseOnBlockTarget::new, true
        ).toEntry(),
        new DispenserBehaviorTarget.Creator.EntityPredicated(
            EntityOnInteractTarget.NAME, EntityOnInteractTarget::new, true
        ).toEntry(),
        new DispenserBehaviorTarget.Creator.EntityPredicated(
            ItemUseOnEntityTarget.NAME, ItemUseOnEntityTarget::new, true
        ).toEntry(),
        new DispenserBehaviorTarget.Creator.Unpredicated(
            ItemUseTarget.NAME, ItemUseTarget::getInstance, true
        ).toEntry(),
        new DispenserBehaviorTarget.Creator.BlockAndEntityPredicated(
            DropTarget.NAME, DropTarget::new, true
        ).toEntry(),
        // FailTarget would be a redundant default alongside DropTarget
        new DispenserBehaviorTarget.Creator.BlockAndEntityPredicated(
            FailTarget.NAME, FailTarget::new, false
        ).toEntry()
    );

    private static final ImmutableSet<DispenserBehaviorTarget.Creator> DEFAULT_TARGET_CREATORS =
        TARGET_CREATORS.values().stream()
            .filter(DispenserBehaviorTarget.Creator::isDefault)
            .collect(ImmutableSet.toImmutableSet());

    private static final Codec<DispenserBehaviorTarget.Creator> TARGET_CREATOR_CODEC = Codecs.NON_EMPTY_STRING
        .flatXmap(
            targetName -> {
                final DispenserBehaviorTarget.Creator creator = TARGET_CREATORS.get(targetName);
                if (creator == null) {
                    return DataResult.error(() -> "Unrecognized target: " + targetName);
                } else {
                    return DataResult.success(creator);
                }
            },
            creator -> DataResult.success(creator.name())
        );

    private static final Codec<ImmutableSet<DispenserBehaviorTarget.Creator>> TARGET_CREATORS_CODEC =
        CodecUtil.immutableSetOf(TARGET_CREATOR_CODEC).orElse(DEFAULT_TARGET_CREATORS);

    private static final Codec<Integer> MIN_OR_MAX_CODEC = Codecs.NON_EMPTY_STRING
        .flatXmap(
            string -> switch (string) {
                case Keys.MIN -> DataResult.success(MIN_PRIORITY);
                case Keys.MAX -> DataResult.success(MAX_PRIORITY);
                default -> DataResult.error(() ->
                    "Unrecognized priority string: \"%s\"; expected \"%s\" or \"%s\""
                        .formatted(string, Keys.MIN, Keys.MAX)
                );
            },
            priority -> switch (priority) {
                case MIN_PRIORITY -> DataResult.success(Keys.MIN);
                case MAX_PRIORITY -> DataResult.success(Keys.MAX);
                default -> DataResult.error(() -> "Priority is neither min nor max: " + priority);
            }
        );

    private static final Codec<Integer> PRIORITY_CODEC = Codec.withAlternative(
        Codec.intRange(MIN_PRIORITY, MAX_PRIORITY),
        MIN_OR_MAX_CODEC
    );

    public static final MapCodec<Configurator> CODEC = RecordCodecBuilder.mapCodec(instance ->
        instance.group(
            TARGET_CREATORS_CODEC.fieldOf(Keys.TARGETS).forGetter(Configurator::targetCreators),
            TaggableId.SET_CODEC.fieldOf(Keys.ITEMS).forGetter(Configurator::itemIds),
            PRIORITY_CODEC.optionalFieldOf(Keys.PRIORITY, DEFAULT_PRIORITY).forGetter(Configurator::priority),

            CodecUtil.optionalWithAlternative(TaggableId.SET_CODEC, Keys.BLOCK_ALLOW_LIST, Keys.BLOCK_WHITE_LIST)
                .forGetter(Configurator::allowedBlockIds),
            CodecUtil.optionalWithAlternative(TaggableId.SET_CODEC, Keys.BLOCK_DENY_LIST, Keys.BLOCK_BLACK_LIST)
                .forGetter(Configurator::deniedBlockIds),
            CodecUtil.optionalWithAlternative(TaggableId.SET_CODEC, Keys.ENTITY_ALLOW_LIST, Keys.ENTITY_WHITE_LIST)
                .forGetter(Configurator::allowedEntityIds),
            CodecUtil.optionalWithAlternative(TaggableId.SET_CODEC, Keys.ENTITY_DENY_LIST, Keys.ENTITY_BLACK_LIST)
                .forGetter(Configurator::deniedEntityIds)
        ).apply(instance, Configurator::new)
    );

    public GenericDispenserBehavior.Holder resolver(Identifier id, DynamicRegistryManager registries) {
        return new GenericDispenserBehavior.Holder(id, this, registries);
    }

    public interface Keys {
        String TARGETS = "targets";

        String PRIORITY = "priority";

        String MAX = "MAX";
        String MIN = "MIN";

        String ITEMS = "items";

        String BLOCK_ALLOW_LIST = "block_allow_list";
        String BLOCK_DENY_LIST = "block_deny_list";

        String ENTITY_ALLOW_LIST = "entity_allow_list";
        String ENTITY_DENY_LIST = "entity_deny_list";

        String BLOCK_WHITE_LIST = "block_white_list";
        String BLOCK_BLACK_LIST = "block_black_list";

        String ENTITY_WHITE_LIST = "entity_white_list";
        String ENTITY_BLACK_LIST = "entity_black_list";
    }
}
