package net.sssubtlety.dispenser_configurator.behavior;

import net.sssubtlety.dispenser_configurator.DispenserConfigurator;
import net.sssubtlety.dispenser_configurator.behavior.util.PriorityBehaviorMultiMap;

import net.fabricmc.fabric.api.resource.IdentifiableResourceReloadListener;

import net.minecraft.item.ItemStack;
import net.minecraft.registry.DynamicRegistryManager;
import net.minecraft.registry.ResourceFileNamespace;
import net.minecraft.resource.JsonDataLoader;
import net.minecraft.resource.ResourceManager;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.profiler.Profiler;

import java.util.*;

public final class ConfiguratorManager extends JsonDataLoader<Configurator>
        implements IdentifiableResourceReloadListener {
    public static final ConfiguratorManager INSTANCE = new ConfiguratorManager("dispenser_configurators");

    private PriorityBehaviorMultiMap behaviors = PriorityBehaviorMultiMap.EMPTY;

    private Map<Identifier, Configurator> cachedData;

    private ConfiguratorManager(String dataType) {
        super(Configurator.CODEC.codec(), ResourceFileNamespace.json(dataType));
    }

    // return true iff vanilla behavior was replaced (should not be dispensed)
    public boolean tryReplaceDispensation(BlockPointer blockPointer, ItemStack stack, int i) {
        for (final GenericDispenserBehavior behavior : this.behaviors.get(stack)) {
            stack = behavior.dispenseSilently(blockPointer, stack);
            blockPointer.blockEntity().setStack(i, stack);
            if (behavior.isSuccess()) {
                return true;
            }
        }

        return false;
    }

    @Override
    protected void apply(Map<Identifier, Configurator> cache, ResourceManager manager, Profiler profiler) {
        this.cachedData = cache;
    }

    public void resolveBehaviors(DynamicRegistryManager registries) {
        // no configurators enabled
        if (this.cachedData == null) {
            return;
        }

        final PriorityBehaviorMultiMap.Builder mapBuilder = PriorityBehaviorMultiMap.builder();
        this.cachedData.forEach((id, data) -> mapBuilder.resolveAndMap(data.resolver(id, registries)));

        this.behaviors = mapBuilder.build();

        this.cachedData = null;
    }

    @Override
    public Identifier getFabricId() {
        return DispenserConfigurator.ID;
    }
}
