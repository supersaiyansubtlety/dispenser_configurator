package net.sssubtlety.dispenser_configurator.behavior;

import com.google.common.collect.ImmutableList;

import net.minecraft.block.Block;
import net.minecraft.block.dispenser.DispenserBlock;
import net.minecraft.block.dispenser.FallibleItemDispenserBehavior;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.registry.DynamicRegistryManager;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.RegistryKeys;
import net.minecraft.util.Identifier;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;

import net.sssubtlety.dispenser_configurator.DispenserConfigurator;
import net.sssubtlety.dispenser_configurator.behavior.target.abstraction.DispenserBehaviorTarget;
import net.sssubtlety.dispenser_configurator.util.TaggableId;
import net.sssubtlety.dispenser_configurator.util.predicate.DualSet;

import com.google.common.collect.ImmutableSet;
import org.jetbrains.annotations.Nullable;
import oshi.util.Memoizer;

import com.mojang.serialization.DataResult;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static net.sssubtlety.dispenser_configurator.util.TaggableId.resolveEntries;
import static net.sssubtlety.dispenser_configurator.util.StringUtil.zipLineTabs;

public class GenericDispenserBehavior extends FallibleItemDispenserBehavior {
    protected final ImmutableList<DispenserBehaviorTarget> targets;

    public GenericDispenserBehavior(ImmutableList<DispenserBehaviorTarget> targets) {
        this.targets = targets;
    }

    @Override
    public ItemStack dispenseSilently(BlockPointer dispenserPointer, ItemStack stack) {
        if (dispenserPointer.world().isClient()) {
            return stack;
        }

        this.setSuccess(false);

        final Direction facing = dispenserPointer.state().get(DispenserBlock.FACING);

        final BlockPos facingPos = dispenserPointer.pos().offset(facing);

        final Supplier<BlockHitResult> blockHitResult = Memoizer.memoize(() -> new BlockHitResult(
            dispenserPointer.center(),
            facing, facingPos, false
        ));

        final Supplier<List<Entity>> entities = Memoizer.memoize(() ->
            dispenserPointer.world().getOtherEntities(
                null,
                new Box(facingPos),
                EntityPredicates.EXCEPT_SPECTATOR
            )
        );

        for (final DispenserBehaviorTarget target : this.targets) {
            final Optional<ItemStack> dispenseResult = target.tryDispensing(
                stack, dispenserPointer, facing,
                facingPos, blockHitResult,
                entities
            );

            if (dispenseResult.isPresent()) {
                target.onSuccess(dispenserPointer);
                this.setSuccess(true);

                return dispenseResult.orElseThrow();
            }
        }

        return stack;
    }

    public static class Holder {
        private final Identifier id;

        private final GenericDispenserBehavior behavior;

        private final boolean exclusive;

        private final ImmutableSet<Item> items;

        private final int priority;

        private final DualSet<Block> blockPredicate;
        private final DualSet<EntityType<?>> entityPredicate;

        @SuppressWarnings("OptionalUsedAsFieldOrParameterType")
        private static <E> DualSet<E> resolveDualSet(
            Identifier id,
            Optional<ImmutableSet<TaggableId>> allowedIds, Optional<ImmutableSet<TaggableId>> deniedIds,
            RegistryKey<? extends Registry<E>> registryKey, DynamicRegistryManager registries
        ) {
            final DataResult<List<E>> allowedBlocks =
                resolveEntries(allowedIds.orElse(ImmutableSet.of()), registryKey, registries);
            final DataResult<List<E>> deniedBlocks =
                resolveEntries(deniedIds.orElse(ImmutableSet.of()), registryKey, registries);

            return new DualSet<>(
                flattenAndLogError(id, Configurator.Keys.BLOCK_ALLOW_LIST, allowedBlocks),
                flattenAndLogError(id, Configurator.Keys.BLOCK_DENY_LIST, deniedBlocks)
            );
        }

        private static <E> ImmutableSet<E> flattenAndLogError(
            Identifier id, String key, DataResult<List<E>> entries
        ) {
            return entries
                .resultOrPartial(error ->
                    DispenserConfigurator.LOGGER.error("Error in {} of configurator \"{}\": {}", key, id, error)
                )
                .map(ImmutableSet::copyOf)
                .orElseGet(ImmutableSet::of);
        }

        public Holder(Identifier id, Configurator configurator, DynamicRegistryManager registries) {
            this.id = id;

            this.items = flattenAndLogError(
                id, Configurator.Keys.ITEMS,
                resolveEntries(configurator.itemIds(), RegistryKeys.ITEM, registries)
            );

            this.priority = configurator.priority();

            this.blockPredicate = resolveDualSet(
                id, configurator.allowedBlockIds(), configurator.deniedBlockIds(),
                RegistryKeys.BLOCK, registries
            );

            this.entityPredicate = resolveDualSet(
                id, configurator.allowedEntityIds(), configurator.deniedEntityIds(),
                RegistryKeys.ENTITY_TYPE, registries
            );

            final ImmutableList.Builder<DispenserBehaviorTarget> targetsBuilder = ImmutableList.builder();
            boolean usedBlockPredicate = false;
            boolean usedEntityPredicate = false;
            @Nullable
            DispenserBehaviorTarget exclusiveTarget = null;
            final List<DispenserBehaviorTarget> postExclusiveTargets = new LinkedList<>();
            for (final DispenserBehaviorTarget.Creator creator : configurator.targetCreators()) {
                final DispenserBehaviorTarget.Creator.Creation<DispenserBehaviorTarget> creation =
                    creator.create(this.blockPredicate, this.entityPredicate);

                usedBlockPredicate |= creation.blockPredicated;
                usedEntityPredicate |= creation.entityPredicated;

                final DispenserBehaviorTarget target = creation.target;
                if (exclusiveTarget == null) {
                    if (target.isExclusive()) {
                        exclusiveTarget = target;
                    }

                    targetsBuilder.add(target);
                } else {
                    postExclusiveTargets.add(target);
                }
            }

            if (!postExclusiveTargets.isEmpty()) {
                LOGGER.error(
                    "Exclusive target \"{}\" is followed by more targets; skipping:{}",
                    exclusiveTarget.getName(),
                    zipLineTabs(postExclusiveTargets.stream().map(DispenserBehaviorTarget::getName))
                );
            }

            final ImmutableList<DispenserBehaviorTarget> targets = targetsBuilder.build();

            final Supplier<String> formattedTargetNames = Memoizer.memoize(() ->
                zipLineTabs(targets.stream().map(DispenserBehaviorTarget::getName))
            );
            if (!usedBlockPredicate && !this.blockPredicate.isEmpty()) {
                LOGGER.error(
                    "Block list specified but no target uses block lists; targets:{}",
                    formattedTargetNames.get()
                );
            }

            if (!usedEntityPredicate && !this.entityPredicate.isEmpty()) {
                LOGGER.error(
                    "Entity list specified but no target uses entity lists; targets:{}",
                    formattedTargetNames.get()
                );
            }

            this.behavior = new GenericDispenserBehavior(targets);

            this.exclusive = exclusiveTarget != null;
        }

        public Identifier getId() {
            return this.id;
        }

        public GenericDispenserBehavior getBehavior() {
            return this.behavior;
        }

        public boolean isExclusive() {
            return this.exclusive;
        }

        public ImmutableSet<Item> getItems() {
            return this.items;
        }

        public boolean intersects(Holder other) {
            return this.blockPredicate.intersects(other.blockPredicate) ||
                this.entityPredicate.intersects(other.entityPredicate);
        }

        // 0 means equal priority
        // positive means this has LOWER priority
        // negative means this has HIGHER priority
        public int comparePriority(Holder other) {
            // low priority if accepts a non-bounded set of items
            if (this.priority == other.priority) {
                final boolean thisLow = this.isLowPriority();
                final boolean otherLow = other.isLowPriority();

                if (thisLow == otherLow) {
                    return 0;
                } else if (thisLow) {
                    return 1;
                } else {
                    return -1;
                }
            } else {
                return  other.priority - this.priority;
            }
        }

        private boolean isLowPriority() {
            return this.blockPredicate.allowSet.isEmpty() &&
                this.entityPredicate.allowSet.isEmpty();
        }
    }
}
