package net.sssubtlety.dispenser_configurator.behavior.target;

import net.sssubtlety.dispenser_configurator.behavior.target.abstraction.BlockPredicatedTarget;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;

import java.util.Optional;
import java.util.function.Predicate;

import static net.sssubtlety.dispenser_configurator.behavior.util.DispenserFakePlayer.fakePlayerInteraction;

public class BlockOnUseTarget extends BlockPredicatedTarget {
    public static final String NAME = "BLOCK_USE";

    public BlockOnUseTarget(Predicate<Block> blockPredicate) {
        super(blockPredicate);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Optional<ItemStack> tryAllowedBlockInteraction(
        ItemStack stack, BlockPointer dispenserPointer, BlockState facingState, BlockHitResult hitResult
    ) {
        final Optional<ItemStack> interactionStack = fakePlayerInteraction(
            stack, dispenserPointer,
            (player, heldStack, hand) ->
                facingState.onInteract(heldStack, player.getServerWorld(), player, hand, hitResult)
        );

        if (interactionStack.isPresent()) {
            syncSuccessEffect(dispenserPointer);
        }

        return interactionStack;
    }
}
