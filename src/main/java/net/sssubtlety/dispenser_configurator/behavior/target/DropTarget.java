package net.sssubtlety.dispenser_configurator.behavior.target;

import net.sssubtlety.dispenser_configurator.behavior.target.abstraction.BlockAndEntityPredicatedTarget;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.dispenser.ItemDispenserBehavior;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;

import java.util.Optional;
import java.util.function.Predicate;

public class DropTarget extends BlockAndEntityPredicatedTarget {
    public static final String NAME = "DROP";

    private static final ItemDispenserBehavior DROP_BEHAVIOR = new ItemDispenserBehavior();

    public DropTarget(Predicate<Block> blockPredicate, Predicate<EntityType<?>> entityPredicate) {
        super(blockPredicate, entityPredicate);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public boolean isExclusive() {
        return true;
    }

    @Override
    public void onSuccess(BlockPointer pointer) {
        // DROP_BEHAVIOR plays a sound, so prevent duplicate here
    }

    @Override
    public Optional<ItemStack> tryAllowedBlockInteraction(
        ItemStack stack, BlockPointer dispenserPointer, BlockState facingState, BlockHitResult hitResult
    ) {
        return drop(stack, dispenserPointer);
    }

    @Override
    public Optional<ItemStack> tryAllowedEntityInteraction(ItemStack stack, BlockPointer dispenserPointer, Entity entity) {
        return drop(stack, dispenserPointer);
    }

    private static Optional<ItemStack> drop(ItemStack stack, BlockPointer dispenserPointer) {
        return Optional.of(DROP_BEHAVIOR.dispense(dispenserPointer, stack));
    }
}
