package net.sssubtlety.dispenser_configurator.behavior.target;

import net.sssubtlety.dispenser_configurator.behavior.target.abstraction.BlockAndEntityPredicatedTarget;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;

import java.util.Optional;
import java.util.function.Predicate;

public class FailTarget extends BlockAndEntityPredicatedTarget {
    public static final String NAME = "FAIL";

    public FailTarget(Predicate<Block> blockPredicate, Predicate<EntityType<?>> entityPredicate) {
        super(blockPredicate, entityPredicate);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public boolean isExclusive() {
        return true;
    }

    @Override
    public void onSuccess(BlockPointer pointer) {
        syncFailEffect(pointer);
    }

    @Override
    public Optional<ItemStack> tryAllowedBlockInteraction(
        ItemStack stack, BlockPointer dispenserPointer, BlockState facingState, BlockHitResult hitResult
    ) {
        return Optional.of(stack);
    }

    @Override
    public Optional<ItemStack> tryAllowedEntityInteraction(
        ItemStack stack, BlockPointer dispenserPointer, Entity entity
    ) {
        return Optional.of(stack);
    }
}
