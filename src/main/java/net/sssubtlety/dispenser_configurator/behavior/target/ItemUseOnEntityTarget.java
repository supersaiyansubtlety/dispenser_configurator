package net.sssubtlety.dispenser_configurator.behavior.target;

import net.sssubtlety.dispenser_configurator.behavior.target.abstraction.EntityPredicatedTarget;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPointer;

import java.util.Optional;
import java.util.function.Predicate;

import static net.sssubtlety.dispenser_configurator.behavior.util.DispenserFakePlayer.fakePlayerInteraction;

public class ItemUseOnEntityTarget extends EntityPredicatedTarget {
    public static final String NAME = "USE_ON_ENTITY";

    public ItemUseOnEntityTarget(Predicate<EntityType<?>> entityPredicate) {
        super(entityPredicate);
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Optional<ItemStack> tryAllowedEntityInteraction(
        ItemStack stack, BlockPointer dispenserPointer, Entity entity
    ) {
        if (entity instanceof LivingEntity livingEntity) {
            return fakePlayerInteraction(stack, dispenserPointer, (player, heldStack, hand) ->
                heldStack.getItem().useOnEntity(heldStack, player, livingEntity, hand)
            );
        } else {
            return Optional.empty();
        }
    }
}
