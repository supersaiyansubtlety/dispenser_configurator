package net.sssubtlety.dispenser_configurator.behavior.target;

import net.minecraft.command.argument.EntityAnchorArgumentType;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Vec3d;

import net.sssubtlety.dispenser_configurator.behavior.target.abstraction.DispenserBehaviorTarget;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

import static net.sssubtlety.dispenser_configurator.behavior.util.DispenserFakePlayer.fakePlayerInteraction;

public class ItemUseTarget extends DispenserBehaviorTarget {
    public static final String NAME = "ITEM_USE";

    private static final ItemUseTarget INSTANCE = new ItemUseTarget();

    public static ItemUseTarget getInstance() {
        return INSTANCE;
    }

    private ItemUseTarget() { }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Optional<ItemStack> tryDispensing(
        ItemStack stack, BlockPointer dispenserPointer, Direction facing, BlockPos facingPos,
        Supplier<BlockHitResult> blockHitResult, Supplier<List<Entity>> entities
    ) {
        return fakePlayerInteraction(stack, dispenserPointer, (player, heldStack, hand) -> {
            final Vec3d dispenserCenter = dispenserPointer.center();

            final Vec3d playerFootDest = getFootDest(facing, player, dispenserCenter);

            player.teleport(playerFootDest.getX(), playerFootDest.getY(), playerFootDest.getZ(), false);
            // look at dispenser center
            //   since the eyes were offset in the direction the dispenser is facing,
            //   this makes the player look back at the dispenser center,
            //   facing in the direction opposite `facing`
            player.lookAt(EntityAnchorArgumentType.EntityAnchor.EYES, dispenserCenter);

            return player.interactionManager.interactItem(player, player.getServerWorld(), heldStack, hand);
        });
    }

    private static Vec3d getFootDest(Direction facing, Entity entity, Vec3d dispenserPos) {
        return dispenserPos
            // offset eyes in the direction the dispenser is facing but still within the dispenser's block space
            .add(QuarterDirectionUnits.get(facing))
            .add(0, -entity.getStandingEyeHeight(), 0);
    }

    // cache each direction's unit vector multiplied by 0.25
    private static final class QuarterDirectionUnits {
        private static final Vec3d DOWN = calculate(Direction.DOWN);
        private static final Vec3d UP = calculate(Direction.UP);
        private static final Vec3d NORTH = calculate(Direction.NORTH);
        private static final Vec3d SOUTH = calculate(Direction.SOUTH);
        private static final Vec3d WEST = calculate(Direction.WEST);
        private static final Vec3d EAST = calculate(Direction.EAST);

        public static Vec3d get(Direction direction) {
            return switch (direction) {
                case DOWN -> DOWN;
                case UP -> UP;
                case NORTH -> NORTH;
                case SOUTH -> SOUTH;
                case WEST -> WEST;
                case EAST -> EAST;
            };
        }

        private static Vec3d calculate(Direction direction) {
            return direction.asVec3d().multiply(0.25);
        }
    }
}
