package net.sssubtlety.dispenser_configurator.behavior.target;

import net.sssubtlety.dispenser_configurator.behavior.target.abstraction.DispenserBehaviorTarget;

import net.minecraft.block.dispenser.BlockPlacementDispenserBehavior;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class PlaceBlockTarget extends DispenserBehaviorTarget {
    public static final String NAME = "PLACE_BLOCK";

    public static final PlaceBlockTarget INSTANCE = new PlaceBlockTarget();

    private static final BlockPlacementDispenserBehavior PLACE_BEHAVIOR = new BlockPlacementDispenserBehavior();

    public static PlaceBlockTarget getInstance() {
        return INSTANCE;
    }

    private PlaceBlockTarget() { }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void onSuccess(BlockPointer pointer) {
        // PLACE_BEHAVIOR makes its own sound, so prevent duplicate here
    }

    @Override
    public Optional<ItemStack> tryDispensing(
        ItemStack stack, BlockPointer dispenserPointer, Direction facing, BlockPos facingPos,
        Supplier<BlockHitResult> blockHitResult, Supplier<List<Entity>> entities
    ) {
        final ItemStack dispensationResult = PLACE_BEHAVIOR.dispense(dispenserPointer, stack);

        return PLACE_BEHAVIOR.isSuccess() ? Optional.of(dispensationResult) : Optional.empty();
    }
}
