package net.sssubtlety.dispenser_configurator.behavior.target.abstraction;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;

public abstract class BlockAndEntityPredicatedTarget extends DispenserBehaviorTarget
        implements BlockPredicatedInteractor, EntityPredicatedInteractor {
    private final Predicate<Block> blockPredicate;
    private final Predicate<EntityType<?>> entityPredicate;

    protected BlockAndEntityPredicatedTarget(
        Predicate<Block> blockPredicate, Predicate<EntityType<?>> entityPredicate
    ) {
        this.blockPredicate = blockPredicate;
        this.entityPredicate = entityPredicate;
    }

    public boolean allows(Block block) {
        return this.blockPredicate.test(block);
    }

    public boolean allows(EntityType<?> entityType) {
        return this.entityPredicate.test(entityType);
    }

    @Override
    public final Optional<ItemStack> tryDispensing(
        ItemStack stack, BlockPointer dispenserPointer, Direction facing, BlockPos facingPos,
        Supplier<BlockHitResult> blockHitResult, Supplier<List<Entity>> entities
    ) {
        return this.tryBlockInteraction(stack, dispenserPointer, facingPos, blockHitResult.get())
            .or(() -> this.tryEntityInteractions(stack, dispenserPointer, entities.get()));
    }
}

