package net.sssubtlety.dispenser_configurator.behavior.target.abstraction;

import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;

import java.util.Optional;

public interface BlockPredicatedInteractor {
    default Optional<ItemStack> tryBlockInteraction(
        ItemStack stack, BlockPointer dispenserPointer, BlockPos facingPos, BlockHitResult blockHitResult
    ) {
        final BlockState facingBlockState = dispenserPointer.world().getBlockState(facingPos);

        return this.allows(facingBlockState.getBlock()) ?
            this.tryAllowedBlockInteraction(stack, dispenserPointer, facingBlockState, blockHitResult)
            : Optional.empty();
    }

    boolean allows(Block block);

    Optional<ItemStack> tryAllowedBlockInteraction(
        ItemStack stack, BlockPointer dispenserPointer, BlockState facingState, BlockHitResult hitResult
    );
}
