package net.sssubtlety.dispenser_configurator.behavior.target.abstraction;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;

public abstract class BlockPredicatedTarget extends DispenserBehaviorTarget implements BlockPredicatedInteractor {
    private final Predicate<Block> blockPredicate;

    protected BlockPredicatedTarget(Predicate<Block> blockPredicate) {
        this.blockPredicate = blockPredicate;
    }

    @Override
    public boolean allows(Block block) {
        return this.blockPredicate.test(block);
    }

    @Override
    public final Optional<ItemStack> tryDispensing(
        ItemStack stack, BlockPointer dispenserPointer, Direction facing, BlockPos facingPos,
        Supplier<BlockHitResult> blockHitResult, Supplier<List<Entity>> entities
    ) {
        return this.tryBlockInteraction(stack, dispenserPointer, facingPos, blockHitResult.get());
    }
}
