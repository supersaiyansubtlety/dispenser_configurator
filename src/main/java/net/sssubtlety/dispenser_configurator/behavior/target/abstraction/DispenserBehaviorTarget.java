package net.sssubtlety.dispenser_configurator.behavior.target.abstraction;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.minecraft.world.WorldEvents;
import net.minecraft.world.event.GameEvent;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public abstract class DispenserBehaviorTarget {
    public static void syncSuccessEffect(BlockPointer pointer) {
        syncSuccessEffect(pointer.world(), pointer.pos());
    }

    public static void syncSuccessEffect(World world, BlockPos pos) {
        world.syncWorldEvent(WorldEvents.DISPENSER_DISPENSES, pos, 0);
    }

    public static void syncFailEffect(BlockPointer pointer) {
        syncFailEffect(pointer.world(), pointer.pos());
    }

    public static void syncFailEffect(World world, BlockPos pos) {
        world.syncWorldEvent(WorldEvents.DISPENSER_FAILS, pos, 0);
        world.emitGameEvent(null, GameEvent.BLOCK_ACTIVATE, pos);
    }

    public abstract String getName();

    public abstract Optional<ItemStack> tryDispensing(
        ItemStack stack, BlockPointer dispenserPointer, Direction facing, BlockPos facingPos,
        Supplier<BlockHitResult> blockHitResult, Supplier<List<Entity>> entities
    );

    public boolean isExclusive() {
        return false;
    }

    public void onSuccess(BlockPointer pointer) {
        syncSuccessEffect(pointer);
    }

    public sealed interface Creator {
        Creation<DispenserBehaviorTarget> create(
            Predicate<Block> blockPredicate, Predicate<EntityType<?>> entityPredicate
        );

        String name();

        boolean isDefault();

        default Map.Entry<String, Creator> toEntry() {
            return Map.entry(this.name(), this);
        }

        final class Creation<T extends DispenserBehaviorTarget> {
            public final T target;
            public final boolean blockPredicated;
            public final boolean entityPredicated;

            private Creation(T target, boolean blockPredicated, boolean entityPredicated) {
                this.target = target;
                this.blockPredicated = blockPredicated;
                this.entityPredicated = entityPredicated;
            }
        }

        record Unpredicated(
            String name, Supplier<DispenserBehaviorTarget> factory, boolean isDefault
        ) implements Creator {
            @Override
            public Creation<DispenserBehaviorTarget> create(
                Predicate<Block> blockPredicate, Predicate<EntityType<?>> entityPredicate
            ) {
                return new Creation<>(this.factory.get(), false, false);
            }
        }

        record BlockPredicated(
            String name, Function<Predicate<Block>, BlockPredicatedTarget> factory, boolean isDefault
        ) implements Creator {
            @Override
            public Creation<DispenserBehaviorTarget> create(
                Predicate<Block> blockPredicate, Predicate<EntityType<?>> entityPredicate
            ) {
                return new Creation<>(this.factory.apply(blockPredicate), true, false);
            }
        }

        record EntityPredicated(
            String name, Function<Predicate<EntityType<?>>, EntityPredicatedTarget> factory, boolean isDefault
        ) implements Creator {
            @Override
            public Creation<DispenserBehaviorTarget> create(
                Predicate<Block> blockPredicate, Predicate<EntityType<?>> entityPredicate
            ) {
                return new Creation<>(this.factory.apply(entityPredicate), false, true);
            }
        }

        record BlockAndEntityPredicated(
            String name, BiFunction<Predicate<Block>,
            Predicate<EntityType<?>>, BlockAndEntityPredicatedTarget> factory,
            boolean isDefault
        ) implements Creator {
            @Override
            public Creation<DispenserBehaviorTarget> create(
                Predicate<Block> blockPredicate, Predicate<EntityType<?>> entityPredicate
            ) {
                return new Creation<>(this.factory.apply(blockPredicate, entityPredicate), true, true);
            }
        }
    }
}
