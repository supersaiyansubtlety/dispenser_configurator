package net.sssubtlety.dispenser_configurator.behavior.target.abstraction;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.math.BlockPointer;

import java.util.List;
import java.util.Optional;

public interface EntityPredicatedInteractor {
    default Optional<ItemStack> tryEntityInteractions(
        ItemStack stack, BlockPointer dispenserPointer, List<Entity> entities
    ) {
        for (final Entity entity : entities) {
            if (this.allows(entity.getType())) {
                final Optional<ItemStack> resultStack =
                    this.tryAllowedEntityInteraction(stack, dispenserPointer, entity);
                if (resultStack.isPresent()) {
                    return resultStack;
                }
            }
        }

        return Optional.empty();
    }

    boolean allows(EntityType<?> entityType);

    Optional<ItemStack> tryAllowedEntityInteraction(ItemStack stack, BlockPointer dispenserPointer, Entity entity);
}
