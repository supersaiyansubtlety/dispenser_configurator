package net.sssubtlety.dispenser_configurator.behavior.target.abstraction;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.item.ItemStack;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;

import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.function.Supplier;

public abstract class EntityPredicatedTarget extends DispenserBehaviorTarget implements EntityPredicatedInteractor {
    private final Predicate<EntityType<?>> entityPredicate;

    protected EntityPredicatedTarget(Predicate<EntityType<?>> entityPredicate) {
        this.entityPredicate = entityPredicate;
    }

    @Override
    public boolean allows(EntityType<?> entityType) {
        return this.entityPredicate.test(entityType);
    }

    @Override
    public final Optional<ItemStack> tryDispensing(
        ItemStack stack, BlockPointer dispenserPointer, Direction facing, BlockPos facingPos,
        Supplier<BlockHitResult> blockHitResult, Supplier<List<Entity>> entities
    ) {
        return this.tryEntityInteractions(stack, dispenserPointer, entities.get());
    }
}
