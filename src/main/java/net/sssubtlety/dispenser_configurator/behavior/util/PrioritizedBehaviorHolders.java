package net.sssubtlety.dispenser_configurator.behavior.util;

import net.sssubtlety.dispenser_configurator.behavior.GenericDispenserBehavior;
import net.sssubtlety.dispenser_configurator.util.SortedList;

import java.util.Optional;

public class PrioritizedBehaviorHolders extends SortedList<GenericDispenserBehavior.Holder> {
    public PrioritizedBehaviorHolders() {
        super(GenericDispenserBehavior.Holder::comparePriority);
    }

    /**
     * Adds the passed {@code holder} to this list according to its
     * {@linkplain GenericDispenserBehavior.Holder#comparePriority priority}
     * <i>iff</i> an exclusive {@link GenericDispenserBehavior.Holder} with the same priority hasn't already been added.
     *
     * @return an {@link Optional} holding a conflicting {@link GenericDispenserBehavior.Holder}
     * if a conflict is present, or {@link Optional#empty()} otherwise
     */
    public Optional<GenericDispenserBehavior.Holder> addOrFindConflict(GenericDispenserBehavior.Holder holder) {
        final Finding<GenericDispenserBehavior.Holder> finding = this.find(holder);
        final Optional<GenericDispenserBehavior.Holder> foundResolver = finding.element();
        if (foundResolver.isPresent()) {
            final GenericDispenserBehavior.Holder equalPriorityConfigurator = foundResolver.orElseThrow();
            if (holder.intersects(equalPriorityConfigurator) && equalPriorityConfigurator.isExclusive()) {
                return holder.isExclusive() ?
                    Optional.of(equalPriorityConfigurator) :
                    Optional.empty();
            }
        }

        this.insert(finding.index(), holder);

        return Optional.empty();
    }
}
