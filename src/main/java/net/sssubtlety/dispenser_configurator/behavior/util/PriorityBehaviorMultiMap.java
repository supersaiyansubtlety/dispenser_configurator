package net.sssubtlety.dispenser_configurator.behavior.util;

import net.sssubtlety.dispenser_configurator.behavior.GenericDispenserBehavior;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.registry.Registries;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static net.sssubtlety.dispenser_configurator.DispenserConfigurator.LOGGER;

public class PriorityBehaviorMultiMap {
    public static Builder builder() {
        return new Builder();
    }

    public static final PriorityBehaviorMultiMap EMPTY = new PriorityBehaviorMultiMap(ImmutableMap.of());

    private final ImmutableMap<Item, ImmutableList<GenericDispenserBehavior>> behaviors;

    private PriorityBehaviorMultiMap(ImmutableMap<Item, ImmutableList<GenericDispenserBehavior>> behaviors) {
        this.behaviors = behaviors;
    }

    public Iterable<GenericDispenserBehavior> get(ItemStack stack) {
        final ImmutableList<GenericDispenserBehavior> itemBehaviors = this.behaviors.get(stack.getItem());
        return itemBehaviors == null ?
            Collections.emptyList() :
            itemBehaviors;
    }

    public static class Builder {
        private final Map<Item, PrioritizedBehaviorHolders> holders;

        private Builder() {
            this.holders = new HashMap<>();
        }

        public PriorityBehaviorMultiMap build() {
            return new PriorityBehaviorMultiMap(
                this.holders.entrySet().stream()
                    .collect(ImmutableMap.toImmutableMap(
                        Map.Entry::getKey,
                        entry -> entry.getValue().stream()
                            .map(GenericDispenserBehavior.Holder::getBehavior)
                            .collect(ImmutableList.toImmutableList())
                    ))
            );
        }

        public void resolveAndMap(GenericDispenserBehavior.Holder holder) {
            for (final Item item : holder.getItems()) {
                this.holders
                    .computeIfAbsent(item, unused -> new PrioritizedBehaviorHolders())
                    .addOrFindConflict(holder)
                    .ifPresent(conflict -> LOGGER.error(
                        """
                        Conflicting exclusive configurators found for item: "{}"
                        \tConfigurators: "{}", "{}"
                        \tSkipping: "{}"
                        """,
                        Registries.ITEM.getId(item),
                        conflict.getId(), holder.getId(),
                        holder.getId()
                    ));
            }
        }
    }
}
