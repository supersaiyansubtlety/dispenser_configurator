package net.sssubtlety.dispenser_configurator.mixin;

import com.llamalad7.mixinextras.injector.ModifyExpressionValue;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalIntRef;
import com.llamalad7.mixinextras.sugar.ref.LocalRef;
import net.minecraft.block.BlockState;
import net.minecraft.block.dispenser.DispenserBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPointer;
import net.minecraft.util.math.BlockPos;

import net.sssubtlety.dispenser_configurator.behavior.ConfiguratorManager;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(DispenserBlock.class)
abstract class DispenserBlockMixin {
    @ModifyExpressionValue(
        method = "dispense",
        at = @At(value = "NEW", target = "net/minecraft/util/math/BlockPointer")
    )
    private BlockPointer captureBlockPointer(
        BlockPointer original, @Share("dispenserPointer") LocalRef<BlockPointer> dispenserPointer
    ) {
        dispenserPointer.set(original);
        return original;
    }

    @ModifyExpressionValue(
        method = "dispense",
        at = @At(
            value = "INVOKE",
            target = "Lnet/minecraft/block/entity/DispenserBlockEntity;chooseNonEmptySlot" +
                "(Lnet/minecraft/util/random/RandomGenerator;)I"
        )
    )
    private int captureSlot(int original, @Share("slot") LocalIntRef slot) {
        slot.set(original);

        return original;
    }

    @ModifyExpressionValue(method = "dispense", at = @At(
        value = "INVOKE",
        target = "Lnet/minecraft/block/entity/DispenserBlockEntity;getStack(I)Lnet/minecraft/item/ItemStack;"
    ))
    private ItemStack captureStack(ItemStack original, @Share("stack") LocalRef<ItemStack> stack) {
        stack.set(original);
        return original;
    }

    @Inject(
        method = "dispense", cancellable = true, at = @At(
            value = "INVOKE_ASSIGN",
            target =
                "Lnet/minecraft/block/dispenser/DispenserBlock;getBehaviorForItem(" +
                    "Lnet/minecraft/world/World;" +
                    "Lnet/minecraft/item/ItemStack;" +
                ")Lnet/minecraft/block/dispenser/DispenserBehavior;"
        )
    )
    private void tryConfigurators(
        ServerWorld world, BlockState state, BlockPos pos, CallbackInfo ci,
        @Share("dispenserPointer") LocalRef<BlockPointer> dispenserPointer,
        @Share("slot") LocalIntRef slot,
        @Share("stack") LocalRef<ItemStack> stack
    ) {
        final boolean replaced = ConfiguratorManager.INSTANCE
            .tryReplaceDispensation(dispenserPointer.get(), stack.get(), slot.get());

        if (replaced) {
            ci.cancel();
        }
    }
}
