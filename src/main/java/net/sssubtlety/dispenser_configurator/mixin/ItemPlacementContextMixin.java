package net.sssubtlety.dispenser_configurator.mixin;

import net.minecraft.item.ItemPlacementContext;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.util.math.Direction;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(ItemPlacementContext.class)
abstract class ItemPlacementContextMixin extends ItemUsageContext {
    private ItemPlacementContextMixin() {
        //noinspection DataFlowIssue
        super(null, null, null);
        throw new IllegalStateException("ItemPlacementContextMixin's dummy constructor called!");
    }

    @Inject(method = {"getPlayerLookDirection", "getVerticalPlayerLookDirection"}, at = @At("HEAD"), cancellable = true)
    private void checkNullGetPlayerLookDirection(CallbackInfoReturnable<Direction> cir) {
        if (this.getPlayer() == null) {
            cir.setReturnValue(this.getHitResult().getSide());
        }
    }

    @Inject(method = "getPlacementDirections", at = @At("HEAD"), cancellable = true)
    private void checkNullGetPlacementDirections(CallbackInfoReturnable<Direction[]> cir) {
        if (this.getPlayer() == null) {
            cir.setReturnValue(new Direction[] { this.getHitResult().getSide() });
        }
    }
}
