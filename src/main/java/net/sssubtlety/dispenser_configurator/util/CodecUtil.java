package net.sssubtlety.dispenser_configurator.util;

import com.google.common.collect.ImmutableSet;

import com.mojang.datafixers.util.Either;
import com.mojang.serialization.Codec;
import com.mojang.serialization.MapCodec;

import java.util.List;
import java.util.Optional;

public final class CodecUtil {
    private CodecUtil() { }

    public static <T> Codec<ImmutableSet<T>> immutableSetOf(Codec<T> codec) {
        return codec.listOf().xmap(ImmutableSet::copyOf, List::copyOf);
    }

    public static <T> MapCodec<Optional<T>> optionalWithAlternative(
        Codec<T> codec, String fieldName, String fallbackFieldName
    ) {
        return withAlternative(codec.optionalFieldOf(fieldName), codec.optionalFieldOf(fallbackFieldName));
    }

    public static <T> MapCodec<T> withAlternative(MapCodec<T> codec, MapCodec<T> alternativeCodec) {
        return Codec
            .mapEither(codec, alternativeCodec)
            .xmap(Either::unwrap, Either::left);
    }
}
