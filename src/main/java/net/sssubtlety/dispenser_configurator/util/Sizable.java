package net.sssubtlety.dispenser_configurator.util;

public interface Sizable {
    default boolean isEmpty() {
        return this.size() == 0;
    }

    int size();
}
