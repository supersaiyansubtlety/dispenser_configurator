package net.sssubtlety.dispenser_configurator.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public abstract class SortedList<T> implements StreamIterable<T>, Sizable {
    private final Comparator<T> comparator;

    private final List<T> delegate;

    protected SortedList(Comparator<T> comparator) {
        this.delegate = new ArrayList<>();

        this.comparator = comparator;
    }

    protected Finding<T> find(T t) {
        final int index = Collections.binarySearch(this.delegate, t, this.comparator);

        return index >= 0 ?
            new Finding<>(Optional.of(this.delegate.get(index)), index) :
            new Finding<T>(Optional.empty(), -index - 1);
    }

    protected final void insert(int index, T element) {
        this.delegate.add(index, element);
    }

    @Override
    public int size() {
        return this.delegate.size();
    }

    @Override
    public Stream<T> stream() {
        return this.delegate.stream();
    }

    public record Finding<T>(Optional<T> element, int index) { }
}
