package net.sssubtlety.dispenser_configurator.util;

import org.jetbrains.annotations.NotNull;

import java.util.Iterator;

public interface StreamIterable<T> extends Streamable<T>, Iterable<T> {
    // iterating over the stream ensures no mutation
    @Override
    @NotNull
    default Iterator<T> iterator() {
        return this.stream().iterator();
    }
}
