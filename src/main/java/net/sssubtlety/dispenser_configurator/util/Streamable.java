package net.sssubtlety.dispenser_configurator.util;

import java.util.stream.Stream;

public interface Streamable<T> {
    Stream<T> stream();
}
