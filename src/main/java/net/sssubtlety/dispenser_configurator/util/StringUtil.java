package net.sssubtlety.dispenser_configurator.util;

import net.minecraft.util.Identifier;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.System.lineSeparator;

public class StringUtil {
    public static final String LINE_TAB = lineSeparator() + "\t";

    /**
     * Joins the passed {@code strings} with {@link #LINE_TAB} and prefixes {@link #LINE_TAB} to the result.
     */
    public static String zipLineTabs(Stream<String> strings) {
        return strings.collect(Collectors.joining(LINE_TAB, LINE_TAB, ""));
    }

    public static String toPathSafeString(Identifier id) {
        return id.getNamespace() + '@' + id.getPath();
    }
}
