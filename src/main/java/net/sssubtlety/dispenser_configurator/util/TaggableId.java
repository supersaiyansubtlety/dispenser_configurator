package net.sssubtlety.dispenser_configurator.util;

import com.google.common.collect.ImmutableSet;

import net.minecraft.registry.DynamicRegistryManager;
import net.minecraft.registry.Holder;
import net.minecraft.registry.HolderSet;
import net.minecraft.registry.Registry;
import net.minecraft.registry.RegistryKey;
import net.minecraft.registry.tag.TagKey;
import net.minecraft.util.Identifier;
import net.minecraft.util.dynamic.Codecs;

import com.mojang.serialization.Codec;
import com.mojang.serialization.DataResult;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static net.sssubtlety.dispenser_configurator.util.StringUtil.zipLineTabs;

public record TaggableId(Identifier id, boolean isTag) {
    public static final String TAG_PREFIX = "#";

    public static final Codec<TaggableId> CODEC =
        Codecs.NON_EMPTY_STRING.xmap(TaggableId::of, TaggableId::toString);

    public static final Codec<ImmutableSet<TaggableId>> SET_CODEC = CodecUtil.immutableSetOf(CODEC);

    public static TaggableId of(String idString) {
        final boolean isTag = idString.startsWith(TAG_PREFIX);

        return new TaggableId(
            Identifier.parse(isTag ? idString.substring(1) : idString),
            isTag
        );
    }

    public static <E> DataResult<List<E>> resolveEntries(
        Collection<TaggableId> ids, RegistryKey<? extends Registry<E>> registryKey, DynamicRegistryManager registries
    ) {
        final Registry<E> registry = registries.getLookupOrThrow(registryKey);
        final List<TaggableId> missingIds = new ArrayList<>();
        final List<E> entries = ids.stream()
            .flatMap(taggable -> {
                if (taggable.isTag()) {
                    final Optional<HolderSet.NamedSet<E>> holderSet =
                        registry.getTag(TagKey.of(registryKey, taggable.id()));

                    if (holderSet.isPresent()) {
                        return holderSet.orElseThrow().stream().map(Holder::getValue);
                    } else {
                        missingIds.add(taggable);
                    }
                } else {
                    final Optional<E> entry = registry.getOrEmpty(taggable.id());
                    if (entry.isPresent()) {
                        return Stream.of(entry.orElseThrow());
                    } else {
                        missingIds.add(taggable);
                    }
                }

                return Stream.empty();
            })
            .toList();

        return missingIds.isEmpty() ?
            DataResult.success(entries) :
            DataResult.error(
                () -> "Missing \"%s\" entries:%s".formatted(
                    registryKey.getValue(),
                    zipLineTabs(missingIds.stream().map(TaggableId::toString))
                ),
                entries
            );
    }

    @Override
    public String toString() {
        return (this.isTag ? TAG_PREFIX : "") + this.id.toString();
    }
}
