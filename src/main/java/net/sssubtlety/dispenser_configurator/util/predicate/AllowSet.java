package net.sssubtlety.dispenser_configurator.util.predicate;

import com.google.common.collect.ImmutableSet;

import java.util.Set;
import java.util.stream.Stream;

public class AllowSet<T> implements IterablePredicate<T> {
    private final ImmutableSet<T> allowed;

    private static <T> boolean test(T item, Set<T> allowed) {
        return allowed.isEmpty() || allowed.contains(item);
    }

    public AllowSet(Set<T> allowed) {
        this.allowed = ImmutableSet.copyOf(allowed);
    }

    @Override
    public boolean test(T item) {
        return test(item, this.allowed);
    }

    @Override
    public int size() {
        return this.allowed.size();
    }

    @Override
    public Stream<T> stream() {
        return this.allowed.stream();
    }
}
