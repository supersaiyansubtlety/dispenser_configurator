package net.sssubtlety.dispenser_configurator.util.predicate;

import com.google.common.collect.ImmutableSet;

import java.util.Set;
import java.util.stream.Stream;

public class DenySet<T> implements IterablePredicate<T> {
    private final ImmutableSet<T> denied;

    public DenySet(Set<T> denied) {
        this.denied = ImmutableSet.copyOf(denied);
    }

    @Override
    public boolean test(T t) {
        return !this.denied.contains(t);
    }

    @Override
    public int size() {
        return this.denied.size();
    }

    @Override
    public Stream<T> stream() {
        return this.denied.stream();
    }
}
