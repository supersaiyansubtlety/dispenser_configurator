package net.sssubtlety.dispenser_configurator.util.predicate;

import com.google.common.collect.ImmutableSet;

import net.minecraft.block.Block;
import net.minecraft.entity.EntityType;

import java.util.Set;
import java.util.stream.Stream;

public class DualSet<T> implements IterablePredicate<T> {
    public final AllowSet<T> allowSet;
    public final DenySet<T> denySet;

    public DualSet(Set<T> allowed, Set<T> denied) {
        this.allowSet = new AllowSet<>(allowed);
        this.denySet = new DenySet<>(denied);
    }

    @Override
    public boolean test(T t) {
        return this.denySet.test(t) && this.allowSet.test(t);
    }

    public boolean intersects(DualSet<T> other) {
        for (final T t : this.allowSet) {
            if (this.denySet.test(t) && other.test(t)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public int size() {
        return this.denySet.size() + this.allowSet.size();
    }

    @Override
    public boolean isEmpty() {
        return this.denySet.isEmpty() && this.allowSet.isEmpty();
    }

    @Override
    public Stream<T> stream() {
        return Stream.concat(this.allowSet.stream(), this.denySet.stream());
    }

    public static final class UniversalAcceptor<I> extends DualSet<I> {
        public static final UniversalAcceptor<Block> BLOCK = new UniversalAcceptor<>();
        public static final UniversalAcceptor<EntityType<?>> ENTITY_TYPE = new UniversalAcceptor<>();

        private UniversalAcceptor() {
            super(ImmutableSet.of(), ImmutableSet.of());
        }

        @Override
        public boolean test(I item) {
            return true;
        }
    }
}
