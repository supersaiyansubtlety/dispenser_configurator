package net.sssubtlety.dispenser_configurator.util.predicate;

import net.sssubtlety.dispenser_configurator.util.Sizable;
import net.sssubtlety.dispenser_configurator.util.StreamIterable;

import java.util.function.Predicate;

public interface IterablePredicate<T> extends Predicate<T>, StreamIterable<T>, Sizable { }
